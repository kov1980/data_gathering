import logging

from parsers.parser import Parser
from bs4 import BeautifulSoup  # You can use any other library

logger = logging.getLogger(__name__)


class HtmlParser(Parser):
    wine_count = 0
    review_count = 0
    parsed_ids = []

    def parse_wine_card(self, wine_id, card):
        result = dict()

        self.wine_count += 1
        result['num'] = str(self.wine_count)
        result['id'] = wine_id

        result['name'] = card.find('div', {'class': 'product_card_name'}).text.strip()
        result['name_en'] = card.find('div', {'class': 'product_card_after_name'}).text.strip()

        base_class = 'pr_card_char_item pr_card_char_item--'
        result['volume'] = card.find('div', {'class': base_class + 'VOLUME'}).find('p').text.strip()
        result['country'] = card.find('div', {'class': base_class + 'COUNTRY'}).find('p').text.strip()
        result['price'] = card.find('div', {'class': base_class}).find('p').text.strip()
        result['color'] = card.find('div', {'class': base_class + 'COLOR'}).find('p').text.strip()
        result['alcohol'] = card.find('div', {'class': base_class + 'ALCOHOL'}).find('p').text.strip()
        result['temperature'] = card.find('div', {'class': base_class + 'TEMPERATURE'}).find('p').text.strip()

        try:
            result['sugar'] = card.find('div', {'class': base_class + 'SUGAR'}).find('p').text.strip()
        except:
            logger.warning(' ! ignore without sugar: %s' % result['name_ru'])
            return {}
        try:
            result['manufacturer'] = card.find('div', {'class': base_class + 'MANUFACTURER_NAME'}).\
                find('p').text.strip()
        except:
            result['manufacturer'] = ''
        try:
            result['region'] = card.find('div', {'class': base_class + 'REGION'}).find('p').text.strip()
        except:
            result['region'] = ''
        try:
             sort_list = card.find('div', {'class': base_class + 'SORTA_VINOGRADA'}).find('p').contents
             result['sort'] = ','.join([name.strip() for name in sort_list if isinstance(name, str)])
        except:
            result['sort'] = ''
            # logger.warning(' ! ignore without sort: %s' % result['name_ru'])
            # return {}
        try:
            result['rating_name'] = card.find('div', {'class': base_class + 'rating'}).find('span').text.strip()
            result['rating'] = card.find('div', {'class': base_class + 'rating'}).find('p').text.split('-')[0].strip()
        except:
            result['rating_name'] = ''
            result['rating'] = ''

        card_rank = card.find('div', {'class': 'product_card_rank'}).find('vote').attrs
        result['votes_rating'] = card_rank.get(':rating', '0').strip()
        result['votes_total'] = card_rank.get('total-cnt-text', '0').strip().split(' ')[0]

        card_descr = card.find('div', {'class': 'pr_card_descr_visible'})
        result['description'] = card_descr.contents[0].strip()
        try:
            result['taste'] = card_descr.find('p').text.strip()
        except:
            result['taste'] = ''

        return result

    def parse(self, data):
        """
        Parses html text and extracts field values
        :param data: html text (page)
        :return: a dictionary where key is one
        of defined fields and value is this field's value
        """

        soup = BeautifulSoup(data)

        # Your code here: find an appropriate html element
        card = soup.find('div', {'class': 'product_card_container'})
        wine_id = card.parent.attrs[':product-id']

        wine_dict = dict()
        if wine_id not in self.parsed_ids:
            wine_dict = self.parse_wine_card(wine_id, card)
            self.parsed_ids.append(wine_id)

            compatibility = soup.find_all('div', {'class': 'product_compability_slider__item__text'})
            try:
                wine_dict['product'] = ','.join([p.text.strip() for p in compatibility][:- 1])
            except:
                wine_dict['product'] = ''

            logger.info(' - %s: %s' % (self.wine_count, wine_dict['name']))

        reviews = soup.find_all('div', {'class': 'item_bl_review'})
        if reviews:
            wine_dict['reviews'] = []
            for review in reviews:
                review_dict = dict()
                self.review_count += 1
                review_dict['num'] = str(self.review_count)
                review_dict['wine_id'] = wine_id
                review_dict['reviewer'] = review.find('div', {'class': 'item_bl_review_name'}).text.strip()
                try:
                    review_dict['rating'] = review.find('input', {'class': 'rate_item',
                                                                  'checked': 'checked'}).attrs['value']
                except:
                    review_dict['rating'] = ''
                review_dict['text'] = review.find('div', {'class': 'item_bl_review_txt'}).text.strip()
                wine_dict['reviews'].append(review_dict)
            logger.info('  - add %s reviews' % len(wine_dict['reviews']))

        return wine_dict
