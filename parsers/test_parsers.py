import unittest
import os

from parsers.filter_parser import FilterParser

DATA_DIR = 'data'
SCRAPPER_DIR = DATA_DIR + '/html'


class TestFilterParser(unittest.TestCase):
    def test_parse(self):
        parser = FilterParser(['1', '3', '5'])
        parsed_data = parser.parse({'1': 1, '2': 2, '3': 3, '4': 4, '5': 5})
        self.assertEqual(len(parsed_data), 1)
        self.assertDictEqual(parsed_data[0], {'1': 1, '3': 3, '5': 5})


class TestHtmlParser(unittest.TestCase):

    def test_parse(self):
        from parsers.html_parser import HtmlParser

        parser = HtmlParser([])
        with open(os.path.join(os.path.join(SCRAPPER_DIR, 'test.html')), 'rb') as f:
            res = parser.parse(f.read().decode())

        test_res = {
            'num': '1',
            'id': '9974',
            'name': 'Вино Бардолино Пьеве Сан Вито Валлезелле DOP кр.сух.',
            'name_en': "Bardolino Classico 'Pieve San Vito' Tenuta Valleselle",
            'volume': '0.75',
            'country': 'Италия',
            'price': '799.99',
            'color': 'Красное',
            'alcohol': '13',
            'temperature': '17',
            'sugar': 'Сухое',
            'manufacturer': 'Casa Vitivinicola Tinazzi',
            'region': 'Венето',
            'sort': 'Корвина,Рондинелла,Молинара,Неграра',
            'rating_name': '',
            'rating': '',
            'votes_rating': '3.5',
            'votes_total': '133',
            'description': 'Вина серии ВАЛЛЕЗЕЛЛЕ — это деликатные, бархатистые и полнотелые напитки, обладающие '
                           'сбалансированной и энергичной структурой. Они имеют элегантный ароматический букет с '
                           'характерной пряной нотой и фруктовыми тонами, подчеркнутый ярким и энергичным '
                           'послевкусием. Оценка Луки Марони 2014—89 баллов.',
            'taste': 'Сбалансированный гармоничный вкус с нотами ягод.'}

        assert (res == test_res)


if __name__ == '__main__':
    unittest.main()
